## Installation

### Requirements

  * Python 3.3+ or Python 2.7
  * Linux Ubuntu
  * dlib Library
  * face_recognition python library
  * OpenCV for Python
### Pre-Installation
  

    sudo apt-get install build-essential cmake pkg-config  
    sudo apt-get install libx11-dev libatlas-base-dev  
    sudo apt-get install libgtk-3-dev libboost-python-dev  
    sudo apt-get install python-dev python-pip python3-dev python3-pip  
    # dlib library intallation  
    git clone https://github.com/davisking/dlib.git  
    cd dlib  
    sudo python setup.py install --yes USE_AVX_INSTRUCTIONS --no DLIB_USE_CUDA
    # Bluetooth Library Installation
    sudo apt-get install python-pip libglib2.0-dev
    sudo pip install bluepy
    # GPIO Installation
    sudo apt-get install rpi.gpio
    
    
    
    
### Test Configuration
    python
    import face_recognition
    
    
### Refernces
  * IanHarvey's bluepy library (https://github.com/IanHarvey/bluepy)
  * ageitgey/face_recognition (https://github.com/ageitgey/face_recognition/)
  * https://www.pyimagesearch.com/2015/02/23/install-opencv-and-python-on-your-raspberry-pi-2-and-b/
